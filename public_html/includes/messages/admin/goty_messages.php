<?php
return [
	"goty_ended" => 
	[
		"text" => 'You have ENDED the GOTY award! Top 10 charts should now be generated!'
	],
	"already_accepted" =>
	[
		"text" => "That nomination has already been accepted! Someone else must have gotten there first!",
		"error" => 1
	],
	"category_exists" =>
	[
		"text" => "That category already exists!",
		"error" => 1
	]
];
?>
